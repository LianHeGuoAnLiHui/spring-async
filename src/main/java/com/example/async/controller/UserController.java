package com.example.async.controller;

import com.example.async.service.MailService;
import com.example.async.service.OuterService;
import com.example.async.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Future;

/**
 * @author zhangjiapeng
 * @Package com.example.async.controller
 * @Description: ${todo}
 * @date 2018/11/22 17:06
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private MailService mailService;

    @Autowired
    private OuterService outerService;

    @GetMapping("/register")
    public String register(){
        try {
            long start = System.currentTimeMillis();
            Future<String> userInfoFuture = outerService.getUserInfo();
            userService.insert();
            mailService.sendMail();
            String userInfo = userInfoFuture.get();
            userService.update(userInfo);
            log.info("执行时间"+(System.currentTimeMillis()-start));
            return "注册成功";
        } catch (Exception e) {
           log.error(e.getMessage(),e);
        }
        return "注册失败";
    }
}
