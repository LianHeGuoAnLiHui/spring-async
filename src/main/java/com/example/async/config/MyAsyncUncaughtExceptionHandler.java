package com.example.async.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;

/**
 * @author zhangjiapeng
 * @Package com.example.async.config
 * @Description: ${todo}
 * @date 2018/11/23 15:39
 */
@Slf4j
public class MyAsyncUncaughtExceptionHandler implements AsyncUncaughtExceptionHandler {

    @Override
    public void handleUncaughtException(Throwable ex, Method method, Object... params) {
        log.error("ex=",ex);
        // doSomething....
    }
}
