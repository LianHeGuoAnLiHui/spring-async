package com.example.async.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author zhangjiapeng
 * @Package com.example.async.service
 * @Description: ${todo}
 * @date 2018/11/22 17:03
 */
@Service
@Slf4j
public class MailService {

    @Async("mailExecutor")
    public void sendMail() throws Exception {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        }
        log.info("发送邮件成功");
    }
}
