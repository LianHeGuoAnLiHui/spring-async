package com.example.async.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author zhangjiapeng
 * @Package com.example.async.service
 * @Description: ${todo}
 * @date 2018/11/22 17:03
 */
@Service
@Slf4j
public class UserService {

    public void insert(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("添加用户成功");
    }

    public void update(String userInfo){
        log.info("更新用户成功");
    }
}
